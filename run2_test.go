package pget

import (
	"context"
	"testing"
)

func TestRun2(t *testing.T) {
	if err := New().Run2(context.Background(), &Options{
		Urls:          []string{"https://dldir1.qq.com/qqfile/qq/PCQQ9.5.6/QQ9.5.6.28129.exe"},
		Output:        "test.pkg",
		NumConnection: 1,
		Timeout:       10,
		Quiet:         true,
	}); err != nil {
		t.Fatal(err)
	}
}
